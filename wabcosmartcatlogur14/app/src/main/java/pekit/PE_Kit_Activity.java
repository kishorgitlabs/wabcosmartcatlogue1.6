package pekit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class PE_Kit_Activity extends Activity {
    private FloatingActionButton arrleft;
    private FloatingActionButton arrright;
    public List<String> descrip, flagList, priceflagList;
    private HorizontalScrollView hsv;
    public ProgressDialog loadDialog;
    public List<String> partcodeno;
    public List<String> partnoList;
    public PE_KitAdapter pe_KitAdapter;
    private ListView pekitlistview;
    private ImageView vehicleBackImageView;
    private Alertbox box = new Alertbox(PE_Kit_Activity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private ImageView Cart_Icon;
    private String UserType;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pe__kit_);
        this.hsv = (HorizontalScrollView) findViewById(R.id.horilist);
        arrleft = (FloatingActionButton) findViewById(R.id.left);
        arrright = (FloatingActionButton) findViewById(R.id.right);
        this.vehicleBackImageView = (ImageView) findViewById(R.id.back);
        this.vehicleBackImageView.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });


        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();
        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);
        pekitlistview = (ListView) findViewById(R.id.listView);

        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")))
            Cart_Icon.setVisibility(View.VISIBLE);
        else
            Cart_Icon.setVisibility(View.GONE);



        this.arrright.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() + 1000, hsv.getScrollY());
                Log.v("inside image click", "yeah");
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.VISIBLE);
            }
        });
        this.arrleft.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() - 1000, hsv.getScrollY());
                arrleft.setVisibility(View.GONE);
                arrright.setVisibility(View.VISIBLE);
            }
        });


        Cart_Icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if (cartDTO.getPartCodeList() == null) {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class)
                            .putExtra("from", "CartItem"));
                }
                // startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });



        new RetrieveFileAsyn().execute();
        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(PE_Kit_Activity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(PE_Kit_Activity.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(PE_Kit_Activity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(PE_Kit_Activity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(PE_Kit_Activity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(PE_Kit_Activity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(PE_Kit_Activity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(PE_Kit_Activity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(PE_Kit_Activity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(PE_Kit_Activity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(PE_Kit_Activity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


    }


    class RetrieveFileAsyn extends AsyncTask<Void, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(PE_Kit_Activity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();
        }

        protected String doInBackground(Void... arg0) {
            PEkitDAO connection = new PEkitDAO(PE_Kit_Activity.this);
            PEkitDTO pEkitDTO = new PEkitDTO();
            pEkitDTO = connection.retrieveALL();
            partnoList = pEkitDTO.getpartnoList();
            partcodeno = pEkitDTO.getpartcodeno();
            descrip = pEkitDTO.getdescrip();
            flagList = pEkitDTO.getFlagList();
            priceflagList = pEkitDTO.getPriceflaglist();
            if (partnoList.isEmpty()) {
                return "nodata";
            }
            return "data";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("data")) {
                pe_KitAdapter = new PE_KitAdapter(PE_Kit_Activity.this, partnoList, partcodeno, descrip,
                        flagList, priceflagList);
                pekitlistview.setAdapter(pe_KitAdapter);
                arrright.setVisibility(View.VISIBLE);
                return;
            }
            box.showAlertbox("PE Kit are Not Available !");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadDialog != null) {
            loadDialog.dismiss();
            loadDialog = null;
        }
    }
}
