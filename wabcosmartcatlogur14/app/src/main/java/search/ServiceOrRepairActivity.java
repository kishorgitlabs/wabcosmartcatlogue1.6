package search;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;
import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class ServiceOrRepairActivity extends Activity {
  public List<String> Description;
  public String INPUT_DB_PATH;
  public List<String> Image_List;
  public List<String> PartNoList;
  public String ProductName;
  public ArrayAdapter<String> adapter;
  private FloatingActionButton arrleft;
  private FloatingActionButton arrright;
  private ImageView backImageView;
  private Button go;
  private HorizontalScrollView hsv;
  public String item, SearchType, SearchFrom = "";
  public ProgressDialog loadDialog;
  public List<String> partListList;
  private ListView partListListView;
  private TextView partNo;
  public VehicleServicePartAdapter service;
  private TextView subtitle;
  private EditText textView;
  private TextView title;
  public List<Bitmap> vehicleImage;
  public List<String> vehicleNameList2;
  public List<String> ProductIdList;
  public List<String> ProductNameList;
  public List<String> vehicleIDList;
  public List<String> priceflagList;
  private ImageView Cart_Icon;

  private SharedPreferences myshare;
  private SharedPreferences.Editor edit;
  private String UserType;

  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_service_or_repair);


    myshare = getSharedPreferences("registration", MODE_PRIVATE);
    edit = myshare.edit();


    this.hsv = (HorizontalScrollView) findViewById(R.id.horilist);
    arrleft = (FloatingActionButton) findViewById(R.id.left);
    arrright = (FloatingActionButton) findViewById(R.id.right);
    this.textView = (EditText) findViewById(R.id.textato);

    this.partNo = (TextView) findViewById(R.id.textFontDesign1);
    this.title = (TextView) findViewById(R.id.textView2);
    this.subtitle = (TextView) findViewById(R.id.serviceorrepair);
    Cart_Icon = (ImageView) findViewById(R.id.cart_icon);



    UserType = myshare.getString("usertype", "").toString();
    if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")))
      Cart_Icon.setVisibility(View.VISIBLE);
    else
      Cart_Icon.setVisibility(View.GONE);



    Intent serchintent = getIntent();
    this.textView.setHint(serchintent.getStringExtra("Search_type"));
    this.subtitle.setText(serchintent.getStringExtra("Search_types"));
    this.title.setText(serchintent.getStringExtra("Search_title"));

    SearchType = serchintent.getStringExtra("Search_type").toString();
    SearchFrom = serchintent.getStringExtra("from").toString();

    Cart_Icon.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        CartDAO cartDAO = new CartDAO(getApplicationContext());
        CartDTO cartDTO = cartDAO.GetCartItems();
        if (cartDTO.getPartCodeList() == null) {
          StyleableToast st =
              new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        } else {
          startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class)
              .putExtra("from", "CartItem"));
        }
        // startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
      }
    });

    this.arrright.setOnClickListener(new OnClickListener() {

      public void onClick(View v) {
        hsv.smoothScrollTo(hsv.getScrollX() + 1000, hsv.getScrollY());
        Log.v("inside image click", "yeah");
        arrright.setVisibility(View.GONE);
        arrleft.setVisibility(View.VISIBLE);
      }
    });
    this.arrleft.setOnClickListener(new OnClickListener() {

      public void onClick(View v) {
        hsv.smoothScrollTo(hsv.getScrollX() - 1000, hsv.getScrollY());
        arrleft.setVisibility(View.GONE);
        arrright.setVisibility(View.VISIBLE);
      }
    });

    this.go = (Button) findViewById(R.id.go);

    this.go.setOnClickListener(new OnClickListener() {

      public void onClick(View paramAnonymousView) {
        item = textView.getText().toString();
        Log.v("on text Changed", item);
        if (item.length() >= 3) {
          new RetrieveFileAsynforselect().execute();
        } else {
          textView.setError("Minimum 3 numbers required");
        }
      }
    });

    partListListView = (ListView) findViewById(R.id.vehicle_listView);
    this.backImageView = (ImageView) findViewById(R.id.back);
    this.backImageView.setOnClickListener(new OnClickListener() {

      public void onClick(View v) {
        onBackPressed();
      }
    });

    final ImageView menu = (ImageView) findViewById(R.id.menu);
    menu.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        Context wrapper = new ContextThemeWrapper(ServiceOrRepairActivity.this, R.style.PopupMenu);
        final PopupMenu pop = new PopupMenu(wrapper, v);
        pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

          public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
              case R.id.home:
                startActivity(new Intent(ServiceOrRepairActivity.this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
              case R.id.search:
                startActivity(new Intent(ServiceOrRepairActivity.this, SearchActivity.class));
                break;
              case R.id.notification:
                startActivity(new Intent(ServiceOrRepairActivity.this, NotificationActivity.class));
                break;
              case R.id.vehicle:
                startActivity(new Intent(ServiceOrRepairActivity.this, VehicleMakeActivity.class));
                break;
              case R.id.product:
                startActivity(
                    new Intent(ServiceOrRepairActivity.this, ProductFamilyActivity.class));
                break;
              case R.id.performance:
                startActivity(new Intent(ServiceOrRepairActivity.this, PE_Kit_Activity.class));
                break;
              case R.id.contact:
                startActivity(new Intent(ServiceOrRepairActivity.this, Network_Activity.class));
                break;
              case R.id.askwabco:
                startActivity(new Intent(ServiceOrRepairActivity.this, AskWabcoActivity.class));
                break;
              case R.id.pricelist:
                startActivity(new Intent(ServiceOrRepairActivity.this, PriceListActivity.class));
                break;
              case R.id.update:
                WabcoUpdate update = new WabcoUpdate(ServiceOrRepairActivity.this);
                update.checkVersion();
                break;
            }
            return false;
          }
        });
        pop.setOnDismissListener(new OnDismissListener() {

          @Override
          public void onDismiss(PopupMenu arg0) {
            // TODO Auto-generated method stub
            pop.dismiss();
          }
        });

        pop.inflate(R.menu.main);
        pop.show();
      }
    });


  }


  class RetrieveFileAsynforselect extends AsyncTask<Void, Void, String> {


    protected void onPreExecute() {
      super.onPreExecute();
      loadDialog = new ProgressDialog(ServiceOrRepairActivity.this);
      loadDialog.setMessage("Loading...");
      loadDialog.setProgressStyle(0);
      loadDialog.setCancelable(false);
      loadDialog.show();
    }

    protected String doInBackground(Void... arg0) {
      SearchDAO searchDAO = new SearchDAO(ServiceOrRepairActivity.this);
      SearchDTO searchDTO = new SearchDTO();
      if (item.isEmpty()) {
        return "notxt";
      }
      searchDTO = searchDAO.retriveForServiceAll(item, SearchType);
      Description = searchDTO.getDescription();
      partListList = searchDTO.getPartno_List();
      PartNoList = searchDTO.getPartno_A();
      ProductIdList = searchDTO.getProductIDList();
      ProductNameList = searchDTO.getProductNameList2();
      vehicleNameList2 = searchDTO.getVehicleName();
      vehicleIDList = searchDTO.getVehicleID();
      priceflagList = searchDTO.getPriceflaglist();
      if (Description.isEmpty()) {
        return "Empty";
      }
      return "success";
    }

    @SuppressWarnings("deprecation")
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      loadDialog.dismiss();
      if (result.equals("Empty")) {

        if (SearchFrom.equals("service")) {
          showAlertBox("Service part are not available !");
        } else {
          showAlertBox("Repair kit are not applicable !");
        }
      } else if (result.equals("notxt")) {
        service.clear();
        partListListView.setAdapter(service);
      } else {
        service = new VehicleServicePartAdapter(ServiceOrRepairActivity.this, partListList,
            Description, PartNoList, vehicleIDList, vehicleNameList2, ProductIdList,
            ProductNameList, priceflagList, SearchFrom);
        partListListView.setAdapter(service);
        arrright.setVisibility(View.VISIBLE);
      }
    }

  }

  public ServiceOrRepairActivity() {
    this.vehicleImage = new ArrayList<Bitmap>();
  }

  private void showAlertBox(String msg) {
    // TODO Auto-generated method stub
    Alertbox box = new Alertbox(ServiceOrRepairActivity.this);
    box.showAlertbox(msg);
  }


}
