
package models.dealer.pending.request.query;

import com.google.gson.annotations.SerializedName;
@SuppressWarnings("unused")
public class QueryPart {

    @SerializedName("Description")
    private String mDescription;
    @SerializedName("OrderNumber")
    private String mOrderNumber;
    @SerializedName("PartName")
    private String mPartName;
    @SerializedName("PartNumber")
    private String mPartNumber;
    @SerializedName("Query")
    private String mQuery;
    @SerializedName("QueryDate")
    private String mQueryDate;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String OrderNumber) {
        mOrderNumber = OrderNumber;
    }

    public String getPartName() {
        return mPartName;
    }

    public void setPartName(String PartName) {
        mPartName = PartName;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String PartNumber) {
        mPartNumber = PartNumber;
    }

    public String getQuery() {
        return mQuery;
    }

    public void setQuery(String Query) {
        mQuery = Query;
    }

    public String getQueryDate() {
        return mQueryDate;
    }

    public void setQueryDate(String QueryDate) {
        mQueryDate = QueryDate;
    }

}
