
package models.dealer.pending.responce;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class PartsDetails {

    @SerializedName("data")
    private List<PartsDetailsResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<PartsDetailsResult> getData() {
        return mData;
    }

    public void setData(List<PartsDetailsResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
