
package models.distributor;

import com.google.gson.annotations.SerializedName;

import java.util.List;


@SuppressWarnings("unused")
public class DistributorModel {

    @SerializedName("data")
    private List<DistributorData> mData;
    @SerializedName("result")
    private String mResult;




    public List<DistributorData> getData() {
        return mData;
    }

    public void setData(List<DistributorData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
