
package models.whatsnew;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class WhatsData {

    @SerializedName("date")
    private String mDate;
    @SerializedName("deletestatus")
    private String mDeletestatus;
    @SerializedName("Discription")
    private String mDiscription;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Notificationname")
    private String mNotificationname;
    @SerializedName("status")
    private String mStatus;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDeletestatus() {
        return mDeletestatus;
    }

    public void setDeletestatus(String deletestatus) {
        mDeletestatus = deletestatus;
    }

    public String getDiscription() {
        return mDiscription;
    }

    public void setDiscription(String discription) {
        mDiscription = discription;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getNotificationname() {
        return mNotificationname;
    }

    public void setNotificationname(String notificationname) {
        mNotificationname = notificationname;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
