package directory;

import android.util.Log;
import java.net.URL;

public class CheckOnlineFileModifiedDate {
	private static final String FILENAME = "wabco.zip";
	private String download_file_path;

	public CheckOnlineFileModifiedDate()
	{
		this.download_file_path = "http://brainmagic.info/MobileApps/wabco/db/wabco.zip";
	}
	
	public String checkFileDate() {
		String fileModifieddate = null;
		try {
			fileModifieddate = new URL(this.download_file_path).openConnection().getHeaderField("Last-Modified");
		}catch(Exception e)
		{

			Log.v("in check date", e.getMessage());
		}
		Log.v("date", fileModifieddate);
		return fileModifieddate;
	}
	
	
	
	
	
}
