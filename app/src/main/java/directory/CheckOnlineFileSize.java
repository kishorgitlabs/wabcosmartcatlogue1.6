package directory;

import android.util.Log;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class CheckOnlineFileSize {
    private static final String FILENAME = "wabco.zip";
    private String download_file_path;

    public CheckOnlineFileSize(String url) {
        this.download_file_path = url;
    }

    public double checkFileSize() {
        try {
            return ((Double.valueOf(new URL(this.download_file_path).openConnection().getHeaderField("content-length")).doubleValue() / 1024.0d) / 1024.0d) / 1024.0d;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return 0.0d;
        } catch (IOException e2) {
            e2.printStackTrace();
            Log.v("in check file size", e2.getMessage());
            return 0.0d;
        }
    }
}
