package productfamily;

import static android.content.Context.MODE_PRIVATE;

import java.util.List;

import com.squareup.picasso.Picasso;
import com.wabco.brainmagic.wabco.catalogue.R;

import addcart.Add_TocartActivity;
import alertbox.Alertbox;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vehiclemake.VehicleRepairkitActivity;
import vehiclemake.VehicleServicepartActivity;
import wabco.ZoomActivity;

@SuppressLint({"InflateParams"})
public class ProductFamilyItemAdapter extends ArrayAdapter<String> {
	private List<String> Flag_List;
	private Context context;
	private List<String> partListList;
	private List<String> partTextList;
	private List<String> partdespTextList;
	String productName;
	private List<String> vehicleNameList;
	List<String> logoimage;
	ImageView expandedImageView;
	Animation zoomin;
	ProductItemHolder productItemHolder;
	Animation zoomout;
	private List<String> vehicleIDList,productIDList;
	private SharedPreferences myshare;
	private SharedPreferences.Editor edit;
	private String UserType;


	public ProductFamilyItemAdapter(Context context, List<String> vehicleIDList,List<String> vehicleNameList, List<String> description, List<String> partListList, String productName, List<String> flag_List,List<String> logoimage,List<String> productIDList) {
		super(context, R.layout.activity_productfamily_partlist_text_item, vehicleNameList);
		this.context = context;
		this.partdespTextList = description;
		this.vehicleNameList = vehicleNameList;
		this.vehicleIDList =vehicleIDList;
		this.partListList = partListList;
		this.logoimage = logoimage;
		this.productName = productName;
		this.Flag_List = flag_List;
		this.productIDList = productIDList;

		myshare = context.getSharedPreferences("registration", MODE_PRIVATE);
		edit = myshare.edit();

		UserType = myshare.getString("usertype","").toString();
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.activity_productfamily_partlist_text_item, null);
			productItemHolder = new ProductItemHolder();
			productItemHolder.partno = (TextView) convertView.findViewById(R.id.vehicle_partno_textView);
			productItemHolder.sno = (TextView) convertView.findViewById(R.id.textsno);
			productItemHolder.service = (ImageView) convertView.findViewById(R.id.vehicle_servicepart);
			productItemHolder.repair = (ImageView) convertView.findViewById(R.id.vehicle_repairkit);
			productItemHolder.description = (TextView) convertView.findViewById(R.id.vehicle_desp_textView);
			productItemHolder.manufacture = (TextView) convertView.findViewById(R.id.partno_manu);
			productItemHolder.product = (ImageView) convertView.findViewById(R.id.customImageVIew1);
			productItemHolder.addcart = (Button) convertView.findViewById(R.id.addcart);
			convertView.setTag(productItemHolder);
		} else {
			productItemHolder = (ProductItemHolder) convertView.getTag();
		}

		if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")) && (Flag_List.get(position)).equals("0"))
		{
			productItemHolder.addcart.setVisibility(View.VISIBLE);
		}
		else
		{
			productItemHolder.addcart.setVisibility(View.GONE);
		}
		
		
		productItemHolder.sno.setText(new StringBuilder(String.valueOf(Integer.toString(position + 1))).append(".").toString());
		productItemHolder.description.setText((CharSequence) this.partdespTextList.get(position));
		productItemHolder.partno.setText((CharSequence) this.partListList.get(position));
		productItemHolder.manufacture.setText((CharSequence) this.vehicleNameList.get(position));
	//	productItemHolder.product.setImageBitmap(logoimage.get(position));
		
		if(logoimage.get(position).equals(""))
		{
			Picasso.with(context).load("file:///android_asset/noimagefound.jpg")
					.error(R.drawable.noimagefound).into(productItemHolder.product);

		}
		else 
		{
			Picasso.with(context).load("file:///android_asset/"+logoimage.get(position)+".jpg").error(R.drawable.noimagefound).into(productItemHolder.product);
		}
		
		
		
		
		Log.v("Image file Name", "file:///android_asset/"+logoimage.get(position));
		
		if (((String) this.Flag_List.get(position)).equals("1")) 
		{
			productItemHolder.partno.setTextColor(context.getResources().getColor(R.color.red));
		} 
		else 
		{
			productItemHolder.partno.setTextColor(context.getResources().getColor(R.color.black));
		}
		productItemHolder.service.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				
				

				Intent serv = new Intent(context, VehicleServicepartActivity.class);
				serv.putExtra("vehicleIDList", vehicleIDList.get(position));
				serv.putExtra("PART_NO", (String) partListList.get(position));
				serv.putExtra("vehicleName", (String) vehicleNameList.get(position));
				serv.putExtra("productName", productName);
				serv.putExtra("vehicleIDList", vehicleIDList.get(position));
				serv.putExtra("productIDList", productIDList.get(position));
				Log.v("prodctName in service", productName);
				
				Log.v("Vehicle ID in service",vehicleIDList.get(position).toString());
				
				serv.putExtra("Service", "Service Part");
				context.startActivity(serv);
			}

		});
		productItemHolder.repair.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Intent des = new Intent(context, VehicleRepairkitActivity.class);
				des.putExtra("vehicleIDList", vehicleIDList.get(position));
				des.putExtra("PART_NO", partListList.get(position));
				des.putExtra("vehicleName", vehicleNameList.get(position));
				des.putExtra("productName", productName);
				des.putExtra("vehicleIDList", vehicleIDList.get(position));
				des.putExtra("productIDList", productIDList.get(position));
				Log.v("prodctName in Repaire", productName);
				context.startActivity(des);
			}
		});
		
		
		
		 expandedImageView =  (ImageView) ((Activity) context).findViewById(R.id.expanded_image);
		productItemHolder.product.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//expandedImageView.setImageBitmap(logoimage.get(position));
				if(logoimage.get(position).equals(""))
				{
					//showAlert(partdespTextList.get(position)+" Image not found ");
					Toast.makeText(context, partdespTextList.get(position)+" Image not found ", Toast.LENGTH_LONG).show();
				}
				else {
					Intent goZoom = new Intent(context, ZoomActivity.class);
					goZoom.putExtra("ImageName", logoimage.get(position));
					goZoom.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
					context.startActivity(goZoom);
				}
				
			}
		});

		productItemHolder.addcart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent gotocart = new Intent(context, Add_TocartActivity.class);
				gotocart.putExtra("from", "assembly");
				gotocart.putExtra("PART_NO", partListList.get(position));
				gotocart.putExtra("vehicleName", vehicleNameList.get(position));
				gotocart.putExtra("productName", productName);
				gotocart.putExtra("vehicleIDList", vehicleIDList.get(position));
				gotocart.putExtra("productIDList", productIDList.get(position));
				context.startActivity(gotocart);
			}
		});



		return convertView;
	}
	
	private void showAlert(String string) {
		// TODO Auto-generated method stub
		//conadap.clear();
		Alertbox box = new Alertbox(context);
		box.showAlertbox(string);
	}
	
}
