package dealer.uploadorders;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import adapter.Order_confirm_Adapter;
import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import models.quickorder.request.Order;
import models.quickorder.request.Order_Parts;
import models.quickorder.request.QuickOrder;
import models.quickorder.response.OrderResponse;
import network.NetworkConnection;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import persistence.DBHelper;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class DealerUploadOrdersDetailsActivity extends AppCompatActivity {
    private View heade_Layout;
    private ImageView Backbtn, Accountbtn,Cart_Icon,Menu;
    private TextView Tittle,Username;
    //Offline_Orderlist_Adapter Adapter;
    private Button Confirm;
    private TextView Distributor_Address, Distributor_name;
    private ListView listview;
    private ProgressDialog progressDialog;
    private SQLiteDatabase db;
    private Cursor c;
    private DBHelper dbHelper;
    private ArrayList<String> PartNumList, DescriptionList, QuantityList, PartNameList, PartCodeList;
    private ArrayList<Integer> Pricelist, TotalList;
    private String GrandTotal;
    private TextView totalMrp;

    private QuickOrder quickOrder = new QuickOrder();
    private SharedPreferences Dealer_share;
    private SharedPreferences.Editor Dealer_edit;
    private Alertbox alert = new Alertbox(DealerUploadOrdersDetailsActivity.this);
    private Order_confirm_Adapter Adapter;
    private CartDTO cartDTO = new CartDTO();
    private String TotalPartsQty;
    NetworkConnection isnet = new NetworkConnection(DealerUploadOrdersDetailsActivity.this);
    private  String Dis_name,Dis_id, Dis_address;
    private ImageView Menubtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_upload_orders_details);
        Confirm = (Button) findViewById(R.id.confirm);

        heade_Layout =  findViewById(R.id.header_layout);
        Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
        Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);
        Accountbtn = (ImageView) heade_Layout.findViewById(R.id.account_icon);
        Cart_Icon = (ImageView) heade_Layout.findViewById(R.id.cart_icon);
        Menu = (ImageView) heade_Layout.findViewById(R.id.menu);

        Confirm = (Button) findViewById(R.id.confirm);
        Distributor_Address = (TextView) findViewById(R.id.distributor_address);
        Distributor_name = (TextView) findViewById(R.id.distributor_name);
        totalMrp = (TextView) findViewById(R.id.totalMrp);
        listview = (ListView) findViewById(R.id.listView);



        PartNumList = new ArrayList<String>();
        DescriptionList = new ArrayList<String>();
        QuantityList = new ArrayList<String>();
        PartNameList = new ArrayList<String>();
        PartCodeList = new ArrayList<String>();
        Pricelist = new ArrayList<Integer>();
        TotalList = new ArrayList<Integer>();

        dbHelper = new DBHelper(DealerUploadOrdersDetailsActivity.this);

        progressDialog = new ProgressDialog(DealerUploadOrdersDetailsActivity.this);

        Dealer_share = getSharedPreferences("registration", MODE_PRIVATE);
        Dealer_edit = Dealer_share.edit();

        Dis_id = getIntent().getStringExtra("Dis_id");
        Dis_name = getIntent().getStringExtra("Dis_name");
        Dis_address = getIntent().getStringExtra("Dis_address");
        Tittle.setText("Confirm Order");
        Cart_Icon.setVisibility(View.INVISIBLE);
        Accountbtn.setVisibility(View.INVISIBLE);
        Distributor_name.setText(Dis_name);
        Distributor_Address.setText(Dis_address);



        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isnet.CheckInternet())
                {
                    GetPOJOFor_DistributorAndDealers();
                    GetPOJOFor_OrderedParts(
                            "select partnumber, partname ,description , quantity, price , quantity*price as total  from Offlineorders where distributor_id = "+Dis_id+"");

                    Post_Order();
                }
                else{
                    alert.showAlertbox(getResources().getString(R.string.nointernetmsg));
                }
            }
        });
        new GetPreviewDetails().execute(
                "select partnumber,partcode, partname ,description , quantity, price , quantity*price as total  from Offlineorders where distributor_id = "+Dis_id+"");


        Menubtn = (ImageView) heade_Layout.findViewById(R.id.menu);
        Menubtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(DealerUploadOrdersDetailsActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(DealerUploadOrdersDetailsActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(DealerUploadOrdersDetailsActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(DealerUploadOrdersDetailsActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(DealerUploadOrdersDetailsActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(DealerUploadOrdersDetailsActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(DealerUploadOrdersDetailsActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(DealerUploadOrdersDetailsActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(DealerUploadOrdersDetailsActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(DealerUploadOrdersDetailsActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(DealerUploadOrdersDetailsActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });

    }

    private class GetPreviewDetails extends AsyncTask<String, Void, String> {

        private Cursor cursor;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            db = dbHelper.readDataBase();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                String query = params[0];
                Log.v("confirm query", query);
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        PartNameList.add(cursor.getString(cursor.getColumnIndex("partname")));
                        PartNumList.add(cursor.getString(cursor.getColumnIndex("partnumber")));
                        DescriptionList.add(cursor.getString(cursor.getColumnIndex("description")));
                        QuantityList.add(cursor.getString(cursor.getColumnIndex("quantity")));
                        Pricelist.add(cursor.getInt(cursor.getColumnIndex("price")));
                        TotalList.add(cursor.getInt(cursor.getColumnIndex("total")));
                        PartCodeList.add(cursor.getString(cursor.getColumnIndex("partcode")));


                    } while (cursor.moveToNext());

                    cartDTO.setPartNameList(PartNameList);
                    cartDTO.setPartNoList(PartNumList);
                    cartDTO.setPartdescriptionList(DescriptionList);
                    cartDTO.setPartQuantityList(QuantityList);
                    cartDTO.setPartImageList(PartNumList);
                    cartDTO.setPartCodeList(PartCodeList);
                    cartDTO.setPartTypeList(PartNameList);
                    List<String> newPriceList = new ArrayList<String>(Pricelist.size());
                    for (Integer myInt : Pricelist) {
                        newPriceList.add(String.valueOf(myInt) + ".00");
                    }
                    cartDTO.setPartPriceList(newPriceList);

                    GetGrandTotal();
                    GetTotalOrderedQTY();

                    cursor.close();
                    db.close();
                    return "received";
                } else {
                    cursor.close();
                    db.close();
                    dbHelper.close();
                    return "nodata";
                }
            } catch (Exception e) {
                cursor.close();
                db.close();
                dbHelper.close();
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }
        }

        private void GetTotalOrderedQTY() {

            String query = "select sum(quantity) as TotalQty from Offlineorders where distributor_id = "+Dis_id+"";
            Log.v("TotalQty qurey ", query);
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    TotalPartsQty = cursor.getString(cursor.getColumnIndex("TotalQty"));
                } while (cursor.moveToNext());

            }
        }

        private void GetGrandTotal() {

            String query = "select sum(quantity*price) as Total  from Offlineorders where distributor_id = "+Dis_id+"";
            Log.v("GrandTotal qurey ", query);
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    GrandTotal = cursor.getString(cursor.getColumnIndex("Total"));
                    GrandTotal = GrandTotal + ".00";

                } while (cursor.moveToNext());

            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            switch (result) {
                case "received":
                    Adapter = new Order_confirm_Adapter(DealerUploadOrdersDetailsActivity.this, PartNameList,
                            PartNumList, DescriptionList, QuantityList, Pricelist, TotalList);
                    listview.setAdapter(Adapter);

                    totalMrp.setText(getResources().getString(R.string.total_amount) + " "
                            + getResources().getString(R.string.Rs) + " " + GrandTotal);

                    break;
                case "nodata":

                    final AlertDialog.Builder alertDialog =
                            new AlertDialog.Builder(DealerUploadOrdersDetailsActivity.this);
                    alertDialog.setTitle("WABCO");
                    alertDialog.setMessage(R.string.no_orders_alert);
                    alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

                    alertDialog.show();

                    break;
                default:

                    break;
            }

        }

    }
    private void GetPOJOFor_DistributorAndDealers() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date)); // 2016/11/16 12:08:43

        Order order = new Order();
        order.setOrderDate(dateFormat.format(date));
        order.setOrderedAddress(Dealer_share.getString("address", "no address"));
        order.setOrderedByName(Dealer_share.getString("name", "no name"));
        order.setOrderedMobile(Dealer_share.getString("phone", "no phone"));

            // Distributor Id
            order.setDistributorid(Long.parseLong(Dis_id));

        // Dealer
        order.setDealerid(Long.parseLong(Dealer_share.getString("id", "1L")));
        order.setDeliveryAddress(Dealer_share.getString("address", "no address"));
        order.setDeliveryMobile(Dealer_share.getString("phone", "no phone"));
        order.setDeliveryName(Dealer_share.getString("name", "no name"));
        order.setDeliveryStatus("Pending");
        order.setTotalOrderAmount((long) Float.parseFloat(GrandTotal));
        order.setOrderStatus("New");
        order.setActive("Y");
        order.setTotalOrderedQty(TotalPartsQty);
        quickOrder.setOrder(order);

    }

    private void GetPOJOFor_OrderedParts(String query) {

        Order_Parts[] order_parts;
        db = dbHelper.readDataBase();

        try {
            Log.v("GetPOJOFor_OrderedParts", query);
            c = db.rawQuery(query, null);
            int i = 0;
            order_parts = new Order_Parts[c.getCount()];
            if (c.moveToFirst()) {
                do {
                    order_parts[i] = new Order_Parts();
                    order_parts[i].setPartName(c.getString(c.getColumnIndex("partname")));
                    order_parts[i].setPartNumber(c.getString(c.getColumnIndex("partnumber")));
                    order_parts[i].setDescription(c.getString(c.getColumnIndex("description")));
                    order_parts[i].setQuantity(c.getString(c.getColumnIndex("quantity")));
                    order_parts[i].setPartPrice(Integer.toString(c.getInt(c.getColumnIndex("price"))));
                    order_parts[i].setTotalAmount(Integer.toString(c.getInt(c.getColumnIndex("total"))));
                    order_parts[i].setDeliveryStatus("Pending");
                    order_parts[i].setActive("Y");
                    i++;
                } while (c.moveToNext());
                quickOrder.setOrder_Parts(order_parts);
                c.close();
                db.close();
                dbHelper.close();
            } else {
                c.close();
                db.close();
                dbHelper.close();
            }
        } catch (Exception e) {
            Log.v("Error order parts", e.getMessage());
            c.close();
            db.close();
            dbHelper.close();

        }


    }


    private void Post_Order() {
        final ProgressDialog loading =
                ProgressDialog.show(this, "Placing order", "Please wait...", false, false);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        APIService api = retrofit.create(APIService.class);
        Call<OrderResponse> quickOrderCall = api.PostOrderDetails(quickOrder);

        quickOrderCall.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                loading.dismiss();
                if (response.isSuccessful()) {

                    Log.v("Result", response.body().getResult());

                    if (response.body().getResult().equals("Success")) {
                        PlacedSuccessfull(response.body().getData());
                    } else {
                        alert.showAlertbox(getResources().getString(R.string.server_error));

                    }

                } else {
                    alert.showAlertbox(getResources().getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {

                loading.dismiss();
                t.printStackTrace();
                alert.showAlertbox(getResources().getString(R.string.server_error));
            }
        });
    }
    private void PlacedSuccessfull(String ordernumber) {
        final AlertDialog.Builder alertDialog =
                new AlertDialog.Builder(DealerUploadOrdersDetailsActivity.this);
        alertDialog.setMessage(getString(R.string.order_successfull) + ordernumber);
        alertDialog.setTitle("WABCO");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Delete_QuickOrder();
                ShowWishListAlert();
            }

        });
        alertDialog.show();
    }

    private void ShowWishListAlert() {

        final AlertDialog.Builder alertDialog =
                new AlertDialog.Builder(DealerUploadOrdersDetailsActivity.this);
        alertDialog.setMessage(R.string.saving_to_wishlist);
        alertDialog.setTitle("WABCO");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                SaveWhislListItems();
            }

        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                startActivity(new Intent(DealerUploadOrdersDetailsActivity.this, MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        alertDialog.show();

    }

    private void SaveWhislListItems() {
        CartDAO cartDAO = new CartDAO(DealerUploadOrdersDetailsActivity.this);
        cartDAO.addWishListFromQuickOrder(cartDTO);
    }

    private void Delete_QuickOrder()
    {try {
        db = dbHelper.readDataBase();
        db.execSQL("delete from  Offlineorders where distributor_id = "+Dis_id+"");
        db.close();
    }catch (Exception ex)
    {
        Log.v("Delete", ex.getMessage());
    }
    }
}

