package vehiclemake;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import persistence.DBHelper;


public class VehicleDAO {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public VehicleDAO(Context context) {
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }

    public void openDatabase() {
        this.db = this.dbHelper.readDataBase();
    }

    public void closeDatabase() {
        if (this.db != null) {
            this.db.close();
        }
    }

    public VehicleDTO retrieveALL() {
        List<String> vehicleNameList = new ArrayList<String>();
        List<String> vehicleIDList = new ArrayList<String>();
        List<String> productIDList = new ArrayList<String>();
        Log.v("Called", " retrive all");
        VehicleDTO vehicleDAO = new VehicleDTO();
        openDatabase();
        Log.v("Called", "openDatabase");
        Cursor cursor = this.db.rawQuery("select distinct Vehicle_id,Vehicle_name from vehicle_make  order by Vehicle_name asc", null);
        if (cursor.moveToFirst()) {
            do {
                vehicleIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
                vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
                Log.v("name", cursor.getString(cursor.getColumnIndex("Vehicle_name")));
                Log.v("id", cursor.getString(cursor.getColumnIndex("Vehicle_id")));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        vehicleDAO.setVehicleIDList(vehicleIDList);
        vehicleDAO.setVehicleNameList(vehicleNameList);
        return vehicleDAO;
    }

    public VehicleDTO retrieveProductFamilyList(String vehicleProductFamilyID) {
        List<String> productNameList = new ArrayList<String>();
        List<String> productIDList = new ArrayList<String>();
        VehicleDTO vehicleDAO = new VehicleDTO();
        openDatabase();
        Cursor cursor = this.db.rawQuery("select distinct p.Vehicle_id ,a.Vehicle_Product_id,a.ProducutName from product a,products_assemply p  where p.Vehicle_id=? and a.Vehicle_Product_id=p.Vehicle_Product_id order by ProducutName asc", new String[]{vehicleProductFamilyID});
        if (cursor.moveToFirst()) {
            do {
                productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
                productNameList.add(cursor.getString(cursor.getColumnIndex("ProducutName")));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        vehicleDAO.setProductIDList(productIDList);
        vehicleDAO.setProductNameList(productNameList);
        return vehicleDAO;
    }

    public VehicleDTO retrievePartItemList(String vehiclePartItemID, String vehicleID, String productname, String vehiclename) {
        List<String> vehiclePartNoID = new ArrayList<String>();
        List<String> vehiclePartProductNOList = new ArrayList<String>();
        List<String> vehiclePartNoList = new ArrayList<String>();
        List<String> vehicleDescriptionList = new ArrayList<String>();
        List<String> vehicleImageList = new ArrayList<String>();
        List<String> vehicleFlagList = new ArrayList<String>();
        List<String> vehicleNameList = new ArrayList<String>();
        List<String> productNameList = new ArrayList<String>();
        List<String> productIDList = new ArrayList<String>();
        VehicleDTO vehicleDAO = new VehicleDTO();
        openDatabase();
        String query ="Select distinct p.Part_No,p.Vehicle_id,p.Vehicle_Product_id,p.Description,p.ProductImage,p.flag from products_assemply p,product a,vehicle_make v where p.Vehicle_Product_id ='" + vehiclePartItemID + "' and p.Vehicle_id ='" + vehicleID + "'";
        Log.v("Query for  Vehicle part", query);
        
        Cursor cursor = this.db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                vehiclePartNoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
                vehiclePartProductNOList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
                vehicleDescriptionList.add(cursor.getString(cursor.getColumnIndex("Description")));
                vehicleImageList.add(cursor.getString(cursor.getColumnIndex("ProductImage")));
                vehicleFlagList.add(SetFlag(cursor.getString(cursor.getColumnIndex("Part_No"))));
                Log.v("image Name",cursor.getString(cursor.getColumnIndex("ProductImage")));
                productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
                
                productNameList.add(productname);
                vehicleNameList.add(vehiclename);
            } while (cursor.moveToNext());
        }

        closeDatabase();
        vehicleDAO.setPartNoIDList(vehiclePartNoID);
        vehicleDAO.setVehiclePartProductNOList(vehiclePartProductNOList);
        vehicleDAO.setPartNoList(vehiclePartNoList);
        vehicleDAO.setPartDespcriptionList(vehicleDescriptionList);
        vehicleDAO.setPartImageList(vehicleImageList);
        vehicleDAO.setFlagList(vehicleFlagList);
        vehicleDAO.setVehicleNameList(vehicleNameList);
        vehicleDAO.setProductNameList(productNameList);
        vehicleDAO.setProductIDList(productIDList);
        return vehicleDAO;
    }



    // for cart items
    public VehicleDTO retrievePartDetailsforCart(String vehiclePartItemID, String vehicleID, String productname, String vehiclename) {
        List<String> vehiclePartNoID = new ArrayList<String>();
        List<String> vehiclePartProductNOList = new ArrayList<String>();
        List<String> vehiclePartNoList = new ArrayList<String>();
        List<String> vehicleDescriptionList = new ArrayList<String>();
        List<String> vehicleImageList = new ArrayList<String>();
        List<String> vehicleFlagList = new ArrayList<String>();
        List<String> vehicleNameList = new ArrayList<String>();
        List<String> productNameList = new ArrayList<String>();
        List<String> productIDList = new ArrayList<String>();
        List<String> productPriceList = new ArrayList<String>();

        VehicleDTO vehicleDAO = new VehicleDTO();
        openDatabase();
        String query ="Select distinct p.Part_No,p.Vehicle_id,p.Vehicle_Product_id,p.Description,p.ProductImage,p.flag from products_assemply p,product a,vehicle_make v where p.Vehicle_Product_id ='" + vehiclePartItemID + "' and p.Vehicle_id ='" + vehicleID + "'";
        Log.v("Query for  Vehicle part", query);

        Cursor cursor = this.db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {

                if(SetFlag(cursor.getString(cursor.getColumnIndex("Part_No"))).equals("0")) {

                    vehiclePartNoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
                    vehiclePartProductNOList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
                    vehicleDescriptionList.add(cursor.getString(cursor.getColumnIndex("Description")));
                    vehicleImageList.add(cursor.getString(cursor.getColumnIndex("ProductImage")));
                    vehicleFlagList.add(SetFlag(cursor.getString(cursor.getColumnIndex("Part_No"))));
                    Log.v("image Name", cursor.getString(cursor.getColumnIndex("ProductImage")));
                    productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
                    productPriceList.add(GetPriceForCart(cursor.getString(cursor.getColumnIndex("Part_No"))));
                    productNameList.add(productname);
                    vehicleNameList.add(vehiclename);

                }


            } while (cursor.moveToNext());
        }


        closeDatabase();
        vehicleDAO.setPartNoIDList(vehiclePartNoID);
        vehicleDAO.setVehiclePartProductNOList(vehiclePartProductNOList);
        vehicleDAO.setPartNoList(vehiclePartNoList);
        vehicleDAO.setPartDespcriptionList(vehicleDescriptionList);
        vehicleDAO.setPartImageList(vehicleImageList);
        vehicleDAO.setFlagList(vehicleFlagList);
        vehicleDAO.setVehicleNameList(vehicleNameList);
        vehicleDAO.setProductNameList(productNameList);
        vehicleDAO.setProductIDList(productIDList);
        vehicleDAO.setPriceList(productPriceList);
        return vehicleDAO;
    }


    // GetPriceForCart
    public String GetPriceForCart(String partno) {
        // TODO Auto-generated method stub
        openDatabase();

        String query ="select mrplist from pricelist where  Part_No ='" + partno + "' or Partcode  ='" + partno + "'";

        Cursor cursor = this.db.rawQuery(query, null);
        if (cursor.moveToFirst())
        {
            return cursor.getString(cursor.getColumnIndex("mrplist")).toString();
        }
        else
        {
            return "0";
        }

    }



    public VehicleDTO retrieveUsedPartItemList(String part) {
        List<String> vehiclePartNoID = new ArrayList<String>();
        List<String> vehiclePartProductNOList = new ArrayList<String>();
        List<String> vehiclePartNoList = new ArrayList<String>();
        List<String> vehicleDescriptionList = new ArrayList<String>();
        List<String> vehicleImageList = new ArrayList<String>();
        List<String> vehicleFlagList = new ArrayList<String>();
        List<String> vehicleNameList = new ArrayList<String>();
        List<String> productNameList = new ArrayList<String>();
        List<String> productIDList = new ArrayList<String>();
        VehicleDTO vehicleDAO = new VehicleDTO();
        openDatabase();
       
        String query =  "Select distinct p.Part_No,p.Vehicle_id,p.Vehicle_Product_id,p.Description,p.ProductImage,p.flag,v.Vehicle_name,a.ProducutName from products_assemply p,product a,vehicle_make v where p.Part_No = '" + part + "'  and  p.Vehicle_id = v.Vehicle_id and  p.Vehicle_Product_id = a.Vehicle_Product_id";

        //String query ="Select distinct p.Part_No,p.Vehicle_id,p.Vehicle_Product_id,p.Description,p.ProductImage,p.flag,v.Vehicle_name,a.ProducutName products_assemply where Part_No = '" + part + "' and  p.Vehicle_id = v.Vehicle_id and  p.Vehicle_Product_id = a.Vehicle_Product_id";
        Log.v("Query for used part", query);
     
        Cursor cursor = this.db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
            	
                vehiclePartNoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
                vehiclePartProductNOList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
                vehicleDescriptionList.add(cursor.getString(cursor.getColumnIndex("Description")));
                vehicleImageList.add(cursor.getString(cursor.getColumnIndex("ProductImage")));
                vehicleFlagList.add(SetFlag(cursor.getString(cursor.getColumnIndex("Part_No"))));
                productNameList.add(cursor.getString(cursor.getColumnIndex("ProducutName")));
                vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
                productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
                Log.v("image Name",cursor.getString(cursor.getColumnIndex("ProductImage")));
            } while (cursor.moveToNext());
        }
        
        
       
        
        
        
        closeDatabase();
        vehicleDAO.setPartNoIDList(vehiclePartNoID);
        vehicleDAO.setVehiclePartProductNOList(vehiclePartProductNOList);
        vehicleDAO.setPartNoList(vehiclePartNoList);
        vehicleDAO.setPartDespcriptionList(vehicleDescriptionList);
        vehicleDAO.setPartImageList(vehicleImageList);
        vehicleDAO.setFlagList(vehicleFlagList);
        vehicleDAO.setVehicleNameList(vehicleNameList);
        vehicleDAO.setProductNameList(productNameList);
        vehicleDAO.setProductIDList(productIDList);
        
        return vehicleDAO;
    }





    public String SetFlag(String partno)
    {
		// TODO Auto-generated method stub
         //openDatabase();
         
         String query ="select Part_No from pricelist where  Part_No ='" + partno + "' or Partcode  ='" + partno + "'";
         
         Cursor cur = this.db.rawQuery(query, null);
         if (cur.moveToFirst())
         {
             return "0";
         }
         else 
         {
        	  return "1";
		 }
      
	}

	public VehicleDTO retrievesservicepart(String Part_no, String vehicleIDList, String productIDList) {
        List<String> vehiclePartNoList = new ArrayList<String>();
        List<String> vehicleDescriptionList = new ArrayList<String>();
        List<String> vehicleReparKitNo = new ArrayList<String>();
        List<String> vehicleReparKitFlagList = new ArrayList<String>();

        VehicleDTO vehicleDAO = new VehicleDTO();
        openDatabase();
        String query = "select distinct Part_No,Service_part,Description from products_servicepart where Part_No='"+Part_no+"' and Vehicle_id ='"+vehicleIDList+"' and Vehicle_Product_id = '"+productIDList+"'";
        Log.v("Service part Query === ", query);
        Cursor cursor = this.db.rawQuery(query,null);
        if (cursor.moveToFirst()) {
            do {
                if (cursor.getPosition() >= 0) {
                    vehiclePartNoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
                    vehicleReparKitNo.add(cursor.getString(cursor.getColumnIndex("Service_part")));
                    vehicleDescriptionList.add(cursor.getString(cursor.getColumnIndex("Description")));
                    vehicleReparKitFlagList.add(SetFlag(cursor.getString(cursor.getColumnIndex("Service_part"))));
                }
            } while (cursor.moveToNext());
        }
        closeDatabase();
        vehicleDAO.setPartNoList(vehiclePartNoList);
        vehicleDAO.setPartDespcriptionList(vehicleDescriptionList);
        vehicleDAO.setProductservicepartNOList(vehicleReparKitNo);
        vehicleDAO.setFlagList(vehicleReparKitFlagList);

        return vehicleDAO;
    }

    public VehicleDTO retrieves_repairpart(String Part_no, String VehicleId, String productIDList) {
        List<String> vehiclePartNoList = new ArrayList<String>();
        List<String> vehicleDescriptionList = new ArrayList<String>();
        List<String> vehicleReparKitNo = new ArrayList<String>();
        List<String> vehicleReparKitFlagList = new ArrayList<String>();

        VehicleDTO vehicleDAO = new VehicleDTO();
        openDatabase();
        String query = "select distinct Part_No,Repair_Part_Number,Description from products_repairkit where Part_No='"+Part_no+"' and Vehicle_id ='"+VehicleId+"' and Vehicle_Product_id = '"+productIDList+"'";
        Log.v("Repaire Kit Query === ", query);
        		Cursor cursor = this.db.rawQuery(query,null);
        if (cursor.moveToFirst()) {
            do {
                vehiclePartNoList.add(cursor.getString(cursor.getColumnIndex("Part_No")));
                vehicleReparKitNo.add(cursor.getString(cursor.getColumnIndex("Repair_Part_Number")));
                vehicleDescriptionList.add(cursor.getString(cursor.getColumnIndex("Description")));
                vehicleReparKitFlagList.add(SetFlag(cursor.getString(cursor.getColumnIndex("Repair_Part_Number"))));

            } while (cursor.moveToNext());
        }
        closeDatabase();
        vehicleDAO.setPartNoList(vehiclePartNoList);
        vehicleDAO.setPartDespcriptionList(vehicleDescriptionList);
        vehicleDAO.setProductRepairKitNOList(vehicleReparKitNo);
        vehicleDAO.setFlagList(vehicleReparKitFlagList);

        return vehicleDAO;
    }
}
