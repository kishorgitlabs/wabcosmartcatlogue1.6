package vehiclemake;

import java.io.File;
import java.util.List;

import com.squareup.picasso.Picasso;
import com.wabco.brainmagic.wabco.catalogue.R;

import alertbox.Alertbox;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import directory.CheckExternalMemory;
import directory.CheckInternalMemory;
import directory.CheckOnlineFileSize;
import directory.DownloadDatabaseFile;
import network.NetworkConnection;


@SuppressLint({"InflateParams"})
public class VehicleMakeAdapter extends ArrayAdapter<String> {
	private Context context;
	List<String> itemIDList;
	private List<String> vehicleNameList;
	private List<Integer> logoimages;
	String urll ="http://demo.brainmagic.info/wabco/";
	List<Integer> ApplicationPdf;
	private long enqueue;
	ProgressBar pb;
	Dialog dialog;
	int downloadedSize = 0;
	int totalSize = 0;
	private DownloadManager dm;
	private TextView cur_val;
	protected String filename;
	private DownloadDatabaseFile downloadDatabaseFile;
	private SharedPreferences preferences;
	private ProgressDialog updateCheckDialog;
	private Editor editor;
	DownloadDatabaseFile localDownloadDatabaseFile;
	Alertbox box;
	public VehicleMakeAdapter(Context context, List<String> vehicleNameList, List<Integer> logoimages, List<String> itemIDList, List<Integer> ApplicationPdf) {
		super(context, R.layout.activity_vehiclemake_text_item, vehicleNameList);
		this.context = context;
		this.vehicleNameList = vehicleNameList;
		this.logoimages = logoimages;
		this.itemIDList = itemIDList;
		this.ApplicationPdf = ApplicationPdf;
		preferences = context.getSharedPreferences("MyPref", 0);
		editor = preferences.edit();
		localDownloadDatabaseFile = new DownloadDatabaseFile(((Activity)context));
		box = new Alertbox(context);
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		VehicleMakeHolder vehicleMakeHolder;
		boolean  userSelected = false;
		if (convertView == null)
		{
			convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.activity_vehiclemake_text_item, null);
			vehicleMakeHolder = new VehicleMakeHolder();
			vehicleMakeHolder.textItem = (TextView) convertView.findViewById(R.id.make_text);
			vehicleMakeHolder.Product = (Button) convertView.findViewById(R.id.products);
			vehicleMakeHolder.Application = (Button) convertView.findViewById(R.id.applications);
			vehicleMakeHolder.logo = (ImageView) convertView.findViewById(R.id.make_logo);
			convertView.setTag(vehicleMakeHolder);
		} 
		else
		{
			vehicleMakeHolder = (VehicleMakeHolder) convertView.getTag();
		}

		vehicleMakeHolder.textItem.setText((CharSequence) this.vehicleNameList.get(position));


		vehicleMakeHolder.textItem.setText((CharSequence) this.vehicleNameList.get(position));
		Picasso
		.with(context)
		.load(logoimages.get(position))
		.resize(200, 200)
		.into( vehicleMakeHolder.logo);




		vehicleMakeHolder.textItem.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent vehicleProductFamily = new Intent(VehicleMakeAdapter.this.context, VehicleProductFamilyActivity.class);
				vehicleProductFamily.putExtra("VEHICLE_ID", (String) VehicleMakeAdapter.this.itemIDList.get(position));
				vehicleProductFamily.putExtra("VEHICLE_NAME", (String) VehicleMakeAdapter.this.vehicleNameList.get(position));
				VehicleMakeAdapter.this.context.startActivity(vehicleProductFamily);
			}
		});
		vehicleMakeHolder.Product.setOnClickListener(new OnClickListener() {


			public void onClick(View v) {
				Intent vehicleProductFamily = new Intent(VehicleMakeAdapter.this.context, VehicleProductFamilyActivity.class);
				vehicleProductFamily.putExtra("VEHICLE_ID", (String) VehicleMakeAdapter.this.itemIDList.get(position));
				vehicleProductFamily.putExtra("VEHICLE_NAME", (String) VehicleMakeAdapter.this.vehicleNameList.get(position));
				VehicleMakeAdapter.this.context.startActivity(vehicleProductFamily);
			}
		});
		vehicleMakeHolder.Application.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) 
			{
				NetworkConnection net = new NetworkConnection(context);
				if(net.CheckInternet())
				{
					if(vehicleNameList.get(position).equals("Mahindra"))
					{
						box.showAlertbox("No Application Found");
					}
					else
					{
						String com = urll+vehicleNameList.get(position)+".pdf";
						dm = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);
						Log.v("file name == ", com+""+dm);
						String	url = com.replaceAll(" ", "%20");
						Request request = new Request(
								Uri.parse(url));
						enqueue = dm.enqueue(request);

						showDownload();
						com ="";
					}

				}
				else
				{
					box.showAlertbox(context.getResources().getString(R.string.nointernetmsg));
				}


			}

		});
		
		return convertView;

	}




	private void downloadFile(String url)
	{
		CheckExternalMemory localCheckExternalMemory = new CheckExternalMemory();
		CheckInternalMemory localCheckInternalMemory = new CheckInternalMemory();
		CheckOnlineFileSize localCheckOnlineFileSize = new CheckOnlineFileSize(url);
		
		double d1 = localCheckExternalMemory.getExternalMemorySize();
		double d2 = localCheckInternalMemory.getInternalMemorySize();
		double d3 = localCheckOnlineFileSize.checkFileSize();
		if (d2 > d3)
		{
			Log.e("INTERNAL", "START");
			editor.putString("DOWNLOAD_FILE", "INTERNAL");
			editor.commit();
			File localFile3 = new File(context.getDir("wabcodb", 0), filename);
			localDownloadDatabaseFile.showProgress();
			localDownloadDatabaseFile.downloadOnlineFile(localFile3,url);
		}
		else if (d1 <= d3)
		{
			Log.e("EXTERNAL", "START");
			editor.putString("DOWNLOAD_FILE", "EXTERNAL");
			editor.commit();
			File localFile1 = new File(Environment.getExternalStorageDirectory(), "wabco Application");
			if (!localFile1.exists()) {
				localFile1.mkdirs();
			}
			File localFile2 = new File(localFile1, filename);
			localDownloadDatabaseFile.showProgress();
			localDownloadDatabaseFile.downloadOnlineFile(localFile2,url);
		}
	}

	public static double getFolderSize(File paramFile)
	{
		double d = 0.0D;
		if (paramFile.isDirectory())
		{
			File[] arrayOfFile = paramFile.listFiles();
			int i = arrayOfFile.length;
			for (int j = 0;; j++)
			{
				if (j >= i) {
					return d;
				}
				d += getFolderSize(arrayOfFile[j]);
			}
		}
		return paramFile.length();
	}


	public void showDownload() {
        Intent i = new Intent();
        i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
       context. startActivity(i);
    }
    

}
