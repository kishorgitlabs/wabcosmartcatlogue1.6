package wabco;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;

import askwabco.AskWabcoActivity;

import com.wabco.brainmagic.wabco.catalogue.EquivalentActivity;
import com.wabco.brainmagic.wabco.catalogue.R;

import contact.ContactActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pekit.Purpose_Activity;
import pricelist.PriceList;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;

public class Network_Activity extends Activity {

    private ImageView backImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);

        RelativeLayout Distributorlayout = (RelativeLayout) findViewById(R.id.distributorlayout);
        RelativeLayout Servicecenterlayout = (RelativeLayout) findViewById(R.id.servicecenterlayout);
        RelativeLayout Fieldlayout = (RelativeLayout) findViewById(R.id.fieldlayout);

        Distributorlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent go = new Intent(Network_Activity.this, ContactActivity.class);
                go.putExtra("contacttype","Distributor");
                go.putExtra("name","Distributor");
                startActivity(go);

            }
        });

        Servicecenterlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent go = new Intent(Network_Activity.this, ContactActivity.class);
                go.putExtra("contacttype","Authorised Service Centre");
                go.putExtra("name","ASC");
                startActivity(go);

            }
        });
        Fieldlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent go = new Intent(Network_Activity.this, ContactActivity.class);
                go.putExtra("contacttype","Field staff");
                go.putExtra("name","Field staff");
                startActivity(go);

            }
        });



        backImageView = (ImageView) findViewById(R.id.back);

        backImageView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });

        final ImageView menu = (ImageView)findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(Network_Activity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(Network_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(Network_Activity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(Network_Activity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(Network_Activity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(Network_Activity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(Network_Activity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(Network_Activity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(Network_Activity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(Network_Activity.this, PriceListActivity.class));
                                break;
                            case R.id.equivalent:
                                startActivity(new Intent(Network_Activity.this, EquivalentActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(Network_Activity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0)
                    {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });



    }
}
