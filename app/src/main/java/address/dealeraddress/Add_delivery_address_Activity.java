package address.dealeraddress;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import addcart.Order_ConfirmationActivity;
import com.wabco.brainmagic.wabco.catalogue.R;

public class Add_delivery_address_Activity extends AppCompatActivity {

    private Button Delivery,Skip;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_delivery_address);

        Delivery = (Button) findViewById(R.id.delivery);
        Skip = (Button) findViewById(R.id.skip);
        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();



        Delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Add_delivery_address_Activity.this, Select_Existing_delivery_addressActivity.class));

            }
        });

        Skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Add_delivery_address_Activity.this, Order_ConfirmationActivity.class));
            }
        });


    }
}
