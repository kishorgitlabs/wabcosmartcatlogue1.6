package distributor.pending;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

public class Distributor_Pending_Orders_Activity extends AppCompatActivity {


    private TextView Order_number;
    private ImageView Backbtn;
    private View heade_Layout;
    private TextView Tittle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor_pending_orders);
        //ButterKnife.inject(this);

        Order_number = (TextView) findViewById(R.id.order_number);


        heade_Layout = findViewById(R.id.header_layout);
        Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
        Tittle.setText("Pending Orders");

        Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);
        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Order_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Distributor_Pending_Orders_Activity.this,Distributor_Pending_Orders_Details_Activity.class));
            }
        });
    }
}